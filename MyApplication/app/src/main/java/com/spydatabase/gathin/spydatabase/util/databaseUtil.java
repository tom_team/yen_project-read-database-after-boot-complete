package com.spydatabase.gathin.spydatabase.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

/**
 * Created by gathin on 2017/10/23.
 */

public class databaseUtil {
    private static final String TAG = "databaseUtil";
    private static final String DATABASE_PATH = "/sdcard/accounts.db";
    private static final String TABLE_NAME = "accounts";
    private static final String id = "type";


    public static String readDatabase() {

        SQLiteDatabase dbe = null;
        try{
            dbe = SQLiteDatabase.openDatabase(DATABASE_PATH, null, 0);
            Log.d(TAG,"TOMTOM readDatabase EXIST");
        } catch(SQLiteException e){
            e.printStackTrace();
            Log.e(TAG,"TOMTOM readDatabase NOT EXIST");
            return "";
        }

        Cursor c = dbe.rawQuery("SELECT " + id +" FROM " + TABLE_NAME + " ", null);
        String column1 = "";
        if(c.moveToFirst()){
            do{
                //assing values
                column1 = c.getString(0);
                //Do something Here with values
                Log.d(TAG, "column1 : " + column1);

            } while (c.moveToNext());
        }
        c.close();
        dbe.close();
        return column1;
    }
}
