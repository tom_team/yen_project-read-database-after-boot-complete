package com.spydatabase.gathin.spydatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.spydatabase.gathin.spydatabase.util.databaseUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseUtil.readDatabase();
    }
}
