package com.spydatabase.gathin.spydatabase.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

/**
 * Created by gathin on 2017/10/23.
 */

public class CommonReceiver extends BroadcastReceiver {
    private static final String TAG = "CommonReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (action.equals(Intent.ACTION_BOOT_COMPLETED)) {
            Log.d(TAG,"TOMTOM ACTION_BOOT_COMPLETED ");
            databaseUtil.readDatabase();
        } else if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            Log.d(TAG,"TOMTOM CONNECTIVITY_ACTION ");
            databaseUtil.readDatabase();
        }
    }
}